// You have generated a new plugin project without
// specifying the `--platforms` flag. A plugin project supports no platforms is generated.
// To add platforms, run `flutter create -t plugin --platforms <platforms> .` under the same
// directory. You can also find a detailed instruction on how to add platforms in the `pubspec.yaml` at https://flutter.dev/docs/development/packages-and-plugins/developing-packages#plugin-platforms.
library bpscm;

import 'dart:async';

import 'package:flutter/services.dart';

export 'src/models/get_invoice_status_req.dart';
export 'src/models/get_invoice_status_res.dart';
export 'src/models/pos_invoice_edits_req.dart';
export 'src/models/pos_invoice_edits_res.dart';
export 'src/models/pos_invoice_news_req.dart';
export 'src/models/pos_invoice_news_res.dart';
export 'src/invoice_provider.dart';
export 'src/constants.dart';
export 'src/extension.dart';
export 'src/enums.dart';

class Bpscm {
  static const MethodChannel _channel = const MethodChannel('bpscm');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }
}

/// A Calculator.
class Calculator {
  /// Returns [value] plus 1.
  int addOne(int value) => value + 1;
}
