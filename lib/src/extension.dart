import 'dart:math';

import 'package:bpscm/src/invoice_provider.dart';
import 'package:okshop_common/okshop_common.dart';
import 'models/pos_invoice_news_req.dart';

extension ExtensionInvoiceDetail on InvoiceDetail {
  // item 總額
  num get totalAmount {
    this.totalAmt ??= 0.0;
    this.discountAmt ??= 0.0;
    return this.totalAmt + this.discountAmt;
  }

  // 從 remark 取得設定的稅別，預設應稅
  BpscmTaxType get taxType {
    this.remark ??= '';
    return num.tryParse(this.remark)?.bpscmTaxType ?? BpscmTaxType.TX;
  }

  // 免稅銷售額 = 總額
  num get freeSalesAmount {
    if (this.taxType.isFree) {
      return max(this.totalAmount, 0.0);
    }
    return 0.0;
  }

  // 單價 (含稅)
  // 從總額反推單價、銷售額及稅額
  num get priceWithTax {
    // 預設數量 1
    this.qty ??= 1;
    return this.totalAmount / this.qty;
  }

  // 銷售額
  num getSalesAmount(TaxRate taxRate) {
    return this.totalAmount / (1.0 + taxRate.value);
  }

  // 稅額
  // num getTaxAmount(TaxRate taxRate) {
  //   return this.totalAmount - this.getSalesAmount(taxRate);
  // }

  // 特別: 反推單價及稅額
  void refresh(bool needTax) {
    final taxRate = InvoiceProvider.getTaxRate(needTax, this.taxType);
    this.qty ??= 1;
    // 銷售額
    final salesAmount = this.getSalesAmount(taxRate);
    // 稅額 = 總額 - 銷售額
    final taxAmount = this.totalAmount - salesAmount;
    // 金財通需要以下三個數值
    this.unitPrice = (salesAmount / this.qty).roundToDouble();
    this.taxAmt = taxAmount.roundToDouble();
    this.salesAmt = salesAmount.roundToDouble();
  }
}

extension ExtensionPosInvoiceNewsReq on PosInvoiceNewsReq {
  bool get isTaxTypeMix {
    return BpscmTaxType.Mix.value == num.tryParse(this.taxType);
  }

  // 金財通有買家才需計算稅額
  bool get showTax {
    return this.hasBuyer;
    // 列印發票時，混合稅率需計算稅額
    // return this.hasBuyer || this.isTaxTypeMix;
  }

  num get itemsTXSalesAmount {
    final value1 = this.itemsTotalAmount - this.itemsFreeSalesAmount;
    final taxRate = this.showTax ? TaxRate.TX : TaxRate.None;
    final value2 = value1 / (1.0 + taxRate.value);
    return max(0.0, value2);
  }

  // 總銷售額 >= 0
  num get itemsSalesAmount {
    // this.invoiceDetails ??= <InvoiceDetail>[];
    // return this.invoiceDetails.fold(0.0, (previousValue, element) {
    //   return previousValue + element.salesAmt;
    // });
    // 應稅銷售額，可能為負數 (折扣>應稅銷售額)
    final value1 = this.itemsTotalAmount - this.itemsFreeSalesAmount;
    // 總銷售額
    num value2 = 0.0;
    if (value1 >= 0) {
      final taxRate = this.showTax ? TaxRate.TX : TaxRate.None;
      value2 = (value1 / (1.0 + taxRate.value)) + this.itemsFreeSalesAmount;
    } else {
      // 折扣再扣掉免稅銷售額
      value2 = value1 + this.itemsFreeSalesAmount;
    }
    return max(0.0, value2);
  }

  // 免稅銷售額
  num get itemsFreeSalesAmount {
    this.invoiceDetails ??= <InvoiceDetail>[];
    return this.invoiceDetails.fold(0.0, (previousValue, element) {
      return previousValue + element.freeSalesAmount;
    });
  }

  // 稅額 >= 0
  num get itemsTaxAmount {
    // this.invoiceDetails ??= <InvoiceDetail>[];
    // return this.invoiceDetails.fold(0.0, (previousValue, element) {
    //   return previousValue + element.taxAmt;
    // });
    final value1 = this.itemsTotalAmount - this.itemsSalesAmount;
    return max(0.0, value1);
  }

  // 總額 >= 0
  num get itemsTotalAmount {
    this.invoiceDetails ??= [];
    final ret = this.invoiceDetails.fold<num>(
        0.0, (previousValue, element) => previousValue + element.totalAmount);
    return max(0.0, ret);
  }

  bool get hasBuyer {
    this.buyerBan ??= '';
    return this.buyerBan.isNotEmpty;
  }

  // 計算稅率
  void refresh() {
    if (this.itemsTXSalesAmount > 0 && this.itemsFreeSalesAmount > 0) {
      // 必須應稅銷售額及免稅銷售額都有值才是混合稅率
      this.taxType = '${BpscmTaxType.Mix.value}';
    } else {
      if (this.itemsFreeSalesAmount > 0) {
        // 免稅
        this.taxType = '${BpscmTaxType.Free.value}';
      } else {
        // 應稅
        this.taxType = '${BpscmTaxType.TX.value}';
      }
    }
    // item detail
    this.invoiceDetails ??= <InvoiceDetail>[];
    this.invoiceDetails.forEach((element) {
      // 反推單價及稅額
      element.refresh(this.showTax);
    });
    // 從品項計算稅別
    // final ls = this.invoiceDetails.map((e) => e.taxType).toSet();
    // if (ls.length > 1) {
    //   this.taxType = '$kTaxTypeMix';
    // } else if (ls.length == 1) {
    //   this.taxType = '${ls.first}';
    // } else {
    //   this.taxType = '$kTaxTypeNormal';
    // }
    // final taxType = num.parse(this.taxType);
    // final taxRate = InvoiceProvider.getTaxRate(hasBuyer, taxType);
    // 含稅價
    // final priceWithTax = this.totalAmt;
    // 計算未稅價 (如稅率為0，未稅價 == 含稅價)
    // final priceWithoutTax = priceWithTax / (1.0 + taxRate);
    // 計算稅額 (未稅價 * 稅率)
    // final taxAmt = priceWithoutTax * taxRate;
    // 銷售額 (未稅價)
    // final salesAmt = kTaxTypeNormal == taxType ? priceWithoutTax : 0.0;
    // final freeTaxSalesAmt = kTaxTypeFree == taxType ? priceWithoutTax : 0.0;
    //
    // this.apiKey = apiKey;
    // this.posBan = kPosBAN ?? kPosBAN;
    // this.storeCode = storeCode;
    // this.storeName = storeName;
    // this.registerCode = '';
    // this.orderNo = orderNo;
    // this.state = 1;
    // this.sellerBan = sellerBan;
    // this.invoiceNo = invoiceNo;
    // this.invoiceDate = DateFormat("yyyy/MM/dd HH:mm:ss").format(date);
    // this.allowanceDate = DateFormat("yyyy/MM/dd HH:mm:ss").format(date);
    this.invoiceType = hasBuyer ? '25' : '35';
    // this.buyerBan = buyerBan;
    // this.printMark = 'Y';
    // this.memberId = '';
    // this.checkNo = '';
    // this.groupMark = '';
    this.salesAmt = this.itemsTXSalesAmount.roundToDouble();
    this.freeTaxSalesAmt = this.itemsFreeSalesAmount.roundToDouble();
    this.zeroTaxSalesAmt = 0.0;
    this.taxAmt = this.itemsTaxAmount.roundToDouble();
    this.totalAmt = this.itemsTotalAmount.roundToDouble();
    // this.taxType = '$taxType';
    this.taxRate = TaxRate.TX.value;
    // this.discountAmt = 0.0;
    // this.healthyAmt = 0.0;
    // this.carrierType = '';
    // this.carrierId1 = '';
    // this.carrierId2 = '';
    // this.npoBan = '';
    // this.randomNumber = randomNumber;
    // this.mainRemark = '';
  }
}
