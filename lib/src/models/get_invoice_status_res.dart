// To parse this JSON data, do
//
//     final getInvoiceStatusRes = getInvoiceStatusResFromJson(jsonString);

import 'dart:convert';

class GetInvoiceStatusRes {
  GetInvoiceStatusRes({
    this.status,
  });

  String status;

  GetInvoiceStatusRes copyWith({
    String status,
  }) =>
      GetInvoiceStatusRes(
        status: status ?? this.status,
      );

  factory GetInvoiceStatusRes.fromRawJson(String str) =>
      GetInvoiceStatusRes.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory GetInvoiceStatusRes.fromJson(Map<String, dynamic> json) =>
      GetInvoiceStatusRes(
        status: json["Status"] == null ? null : json["Status"],
      );

  Map<String, dynamic> toJson() => {
        "Status": status == null ? null : status,
      };
}
