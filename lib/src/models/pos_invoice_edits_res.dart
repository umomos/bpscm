// To parse this JSON data, do
//
//     final posInvoiceEditsRes = posInvoiceEditsResFromJson(jsonString);

import 'dart:convert';

class PosInvoiceEditsRes {
  PosInvoiceEditsRes({
    this.invoiceNo,
    this.status,
    this.message,
  });

  String invoiceNo;
  String status;
  String message;

  PosInvoiceEditsRes copyWith({
    String invoiceNo,
    String status,
    String message,
  }) =>
      PosInvoiceEditsRes(
        invoiceNo: invoiceNo ?? this.invoiceNo,
        status: status ?? this.status,
        message: message ?? this.message,
      );

  factory PosInvoiceEditsRes.fromRawJson(String str) =>
      PosInvoiceEditsRes.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory PosInvoiceEditsRes.fromJson(Map<String, dynamic> json) =>
      PosInvoiceEditsRes(
        invoiceNo: json["InvoiceNo"] == null ? null : json["InvoiceNo"],
        status: json["Status"] == null ? null : json["Status"],
        message: json["Message"] == null ? null : json["Message"],
      );

  Map<String, dynamic> toJson() => {
        "InvoiceNo": invoiceNo == null ? null : invoiceNo,
        "Status": status == null ? null : status,
        "Message": message == null ? null : message,
      };
}
