// To parse this JSON data, do
//
//     final posInvoiceNewsRes = posInvoiceNewsResFromJson(jsonString);

import 'dart:convert';

class PosInvoiceNewsRes {
  PosInvoiceNewsRes({
    this.invoiceNo,
    this.status,
    this.message,
  });

  String invoiceNo;
  String status;
  String message;

  PosInvoiceNewsRes copyWith({
    String invoiceNo,
    String status,
    String message,
  }) =>
      PosInvoiceNewsRes(
        invoiceNo: invoiceNo ?? this.invoiceNo,
        status: status ?? this.status,
        message: message ?? this.message,
      );

  factory PosInvoiceNewsRes.fromRawJson(String str) =>
      PosInvoiceNewsRes.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory PosInvoiceNewsRes.fromJson(Map<String, dynamic> json) =>
      PosInvoiceNewsRes(
        invoiceNo: json["InvoiceNo"] == null ? null : json["InvoiceNo"],
        status: json["Status"] == null ? null : json["Status"],
        message: json["Message"] == null ? null : json["Message"],
      );

  Map<String, dynamic> toJson() => {
        "InvoiceNo": invoiceNo == null ? null : invoiceNo,
        "Status": status == null ? null : status,
        "Message": message == null ? null : message,
      };
}
