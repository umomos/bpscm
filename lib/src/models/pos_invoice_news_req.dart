// To parse this JSON data, do
//
//     final posInvoiceNewsReq = posInvoiceNewsReqFromJson(jsonString);

import 'dart:convert';

class PosInvoiceNewsReq {
  PosInvoiceNewsReq({
    this.posBan,
    this.apiKey,
    this.sellerBan,
    this.storeCode,
    this.storeName,
    this.registerCode,
    this.orderNo,
    this.state,
    this.invoiceNo,
    this.invoiceDate,
    this.allowanceDate,
    this.buyerBan,
    this.printMark,
    this.memberId,
    this.checkNo,
    this.invoiceType,
    this.groupMark,
    this.salesAmt,
    this.freeTaxSalesAmt,
    this.zeroTaxSalesAmt,
    this.taxAmt,
    this.totalAmt,
    this.taxType,
    this.taxRate,
    this.discountAmt,
    this.healthyAmt,
    this.carrierType,
    this.carrierId1,
    this.carrierId2,
    this.npoBan,
    this.randomNumber,
    this.mainRemark,
    this.invoiceDetails,
  });

  String posBan;
  String apiKey;
  String sellerBan;
  String storeCode;
  String storeName;
  String registerCode;
  String orderNo;
  num state;
  String invoiceNo;
  String invoiceDate;
  String allowanceDate;
  String buyerBan;
  String printMark;
  String memberId;
  String checkNo;
  String invoiceType;
  String groupMark;
  num salesAmt;
  num freeTaxSalesAmt;
  num zeroTaxSalesAmt;
  num taxAmt;
  num totalAmt;
  String taxType;
  num taxRate;
  num discountAmt;
  num healthyAmt;
  String carrierType;
  String carrierId1;
  String carrierId2;
  String npoBan;
  String randomNumber;
  String mainRemark;
  List<InvoiceDetail> invoiceDetails;

  PosInvoiceNewsReq copyWith({
    String posBan,
    String apiKey,
    String sellerBan,
    String storeCode,
    String storeName,
    String registerCode,
    String orderNo,
    num state,
    String invoiceNo,
    String invoiceDate,
    String allowanceDate,
    String buyerBan,
    String printMark,
    String memberId,
    String checkNo,
    String invoiceType,
    String groupMark,
    num salesAmt,
    num freeTaxSalesAmt,
    num zeroTaxSalesAmt,
    num taxAmt,
    num totalAmt,
    String taxType,
    num taxRate,
    num discountAmt,
    num healthyAmt,
    String carrierType,
    String carrierId1,
    String carrierId2,
    String npoBan,
    String randomNumber,
    String mainRemark,
    List<InvoiceDetail> invoiceDetails,
  }) =>
      PosInvoiceNewsReq(
        posBan: posBan ?? this.posBan,
        apiKey: apiKey ?? this.apiKey,
        sellerBan: sellerBan ?? this.sellerBan,
        storeCode: storeCode ?? this.storeCode,
        storeName: storeName ?? this.storeName,
        registerCode: registerCode ?? this.registerCode,
        orderNo: orderNo ?? this.orderNo,
        state: state ?? this.state,
        invoiceNo: invoiceNo ?? this.invoiceNo,
        invoiceDate: invoiceDate ?? this.invoiceDate,
        allowanceDate: allowanceDate ?? this.allowanceDate,
        buyerBan: buyerBan ?? this.buyerBan,
        printMark: printMark ?? this.printMark,
        memberId: memberId ?? this.memberId,
        checkNo: checkNo ?? this.checkNo,
        invoiceType: invoiceType ?? this.invoiceType,
        groupMark: groupMark ?? this.groupMark,
        salesAmt: salesAmt ?? this.salesAmt,
        freeTaxSalesAmt: freeTaxSalesAmt ?? this.freeTaxSalesAmt,
        zeroTaxSalesAmt: zeroTaxSalesAmt ?? this.zeroTaxSalesAmt,
        taxAmt: taxAmt ?? this.taxAmt,
        totalAmt: totalAmt ?? this.totalAmt,
        taxType: taxType ?? this.taxType,
        taxRate: taxRate ?? this.taxRate,
        discountAmt: discountAmt ?? this.discountAmt,
        healthyAmt: healthyAmt ?? this.healthyAmt,
        carrierType: carrierType ?? this.carrierType,
        carrierId1: carrierId1 ?? this.carrierId1,
        carrierId2: carrierId2 ?? this.carrierId2,
        npoBan: npoBan ?? this.npoBan,
        randomNumber: randomNumber ?? this.randomNumber,
        mainRemark: mainRemark ?? this.mainRemark,
        invoiceDetails: invoiceDetails ?? this.invoiceDetails,
      );

  factory PosInvoiceNewsReq.fromRawJson(String str) =>
      PosInvoiceNewsReq.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory PosInvoiceNewsReq.fromJson(Map<String, dynamic> json) =>
      PosInvoiceNewsReq(
        posBan: json["PosBAN"] == null ? null : json["PosBAN"],
        apiKey: json["ApiKey"] == null ? null : json["ApiKey"],
        sellerBan: json["SellerBAN"] == null ? null : json["SellerBAN"],
        storeCode: json["StoreCode"] == null ? null : json["StoreCode"],
        storeName: json["StoreName"] == null ? null : json["StoreName"],
        registerCode:
            json["RegisterCode"] == null ? null : json["RegisterCode"],
        orderNo: json["OrderNo"] == null ? null : json["OrderNo"],
        state: json["State"] == null ? null : json["State"],
        invoiceNo: json["InvoiceNo"] == null ? null : json["InvoiceNo"],
        invoiceDate: json["InvoiceDate"] == null ? null : json["InvoiceDate"],
        allowanceDate:
            json["AllowanceDate"] == null ? null : json["AllowanceDate"],
        buyerBan: json["BuyerBAN"] == null ? null : json["BuyerBAN"],
        printMark: json["PrintMark"] == null ? null : json["PrintMark"],
        memberId: json["MemberId"] == null ? null : json["MemberId"],
        checkNo: json["CheckNo"] == null ? null : json["CheckNo"],
        invoiceType: json["InvoiceType"] == null ? null : json["InvoiceType"],
        groupMark: json["GroupMark"] == null ? null : json["GroupMark"],
        salesAmt: json["SalesAmt"] == null ? null : json["SalesAmt"],
        freeTaxSalesAmt:
            json["FreeTaxSalesAmt"] == null ? null : json["FreeTaxSalesAmt"],
        zeroTaxSalesAmt:
            json["ZeroTaxSalesAmt"] == null ? null : json["ZeroTaxSalesAmt"],
        taxAmt: json["TaxAmt"] == null ? null : json["TaxAmt"],
        totalAmt: json["TotalAmt"] == null ? null : json["TotalAmt"],
        taxType: json["TaxType"] == null ? null : json["TaxType"],
        taxRate: json["TaxRate"] == null ? null : json["TaxRate"].tonum(),
        discountAmt: json["DiscountAmt"] == null ? null : json["DiscountAmt"],
        healthyAmt: json["HealthyAmt"] == null ? null : json["HealthyAmt"],
        carrierType: json["CarrierType"] == null ? null : json["CarrierType"],
        carrierId1: json["CarrierId1"] == null ? null : json["CarrierId1"],
        carrierId2: json["CarrierId2"] == null ? null : json["CarrierId2"],
        npoBan: json["NpoBan"] == null ? null : json["NpoBan"],
        randomNumber:
            json["RandomNumber"] == null ? null : json["RandomNumber"],
        mainRemark: json["MainRemark"] == null ? null : json["MainRemark"],
        invoiceDetails: json["InvoiceDetails"] == null
            ? null
            : List<InvoiceDetail>.from(
                json["InvoiceDetails"].map((x) => InvoiceDetail.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "PosBAN": posBan == null ? null : posBan,
        "ApiKey": apiKey == null ? null : apiKey,
        "SellerBAN": sellerBan == null ? null : sellerBan,
        "StoreCode": storeCode == null ? null : storeCode,
        "StoreName": storeName == null ? null : storeName,
        "RegisterCode": registerCode == null ? null : registerCode,
        "OrderNo": orderNo == null ? null : orderNo,
        "State": state == null ? null : state,
        "InvoiceNo": invoiceNo == null ? null : invoiceNo,
        "InvoiceDate": invoiceDate == null ? null : invoiceDate,
        "AllowanceDate": allowanceDate == null ? null : allowanceDate,
        "BuyerBAN": buyerBan == null ? null : buyerBan,
        "PrintMark": printMark == null ? null : printMark,
        "MemberId": memberId == null ? null : memberId,
        "CheckNo": checkNo == null ? null : checkNo,
        "InvoiceType": invoiceType == null ? null : invoiceType,
        "GroupMark": groupMark == null ? null : groupMark,
        "SalesAmt": salesAmt == null ? null : salesAmt,
        "FreeTaxSalesAmt": freeTaxSalesAmt == null ? null : freeTaxSalesAmt,
        "ZeroTaxSalesAmt": zeroTaxSalesAmt == null ? null : zeroTaxSalesAmt,
        "TaxAmt": taxAmt == null ? null : taxAmt,
        "TotalAmt": totalAmt == null ? null : totalAmt,
        "TaxType": taxType == null ? null : taxType,
        "TaxRate": taxRate == null ? null : taxRate,
        "DiscountAmt": discountAmt == null ? null : discountAmt,
        "HealthyAmt": healthyAmt == null ? null : healthyAmt,
        "CarrierType": carrierType == null ? null : carrierType,
        "CarrierId1": carrierId1 == null ? null : carrierId1,
        "CarrierId2": carrierId2 == null ? null : carrierId2,
        "NpoBan": npoBan == null ? null : npoBan,
        "RandomNumber": randomNumber == null ? null : randomNumber,
        "MainRemark": mainRemark == null ? null : mainRemark,
        "InvoiceDetails": invoiceDetails == null
            ? null
            : List<dynamic>.from(invoiceDetails.map((x) => x.toJson())),
      };
}

class InvoiceDetail {
  InvoiceDetail({
    this.sequenceNo,
    this.itemName,
    this.qty,
    this.unit,
    this.unitPrice,
    this.salesAmt,
    this.taxAmt,
    this.totalAmt,
    this.discountAmt,
    this.healthAmt,
    this.relateNumber,
    this.remark,
  });

  String sequenceNo;
  String itemName;
  num qty;
  String unit;
  num unitPrice;
  num salesAmt;
  num taxAmt;
  num totalAmt;
  num discountAmt;
  num healthAmt;
  String relateNumber;
  String remark;

  InvoiceDetail copyWith({
    String sequenceNo,
    String itemName,
    num qty,
    String unit,
    num unitPrice,
    num salesAmt,
    num taxAmt,
    num totalAmt,
    num discountAmt,
    num healthAmt,
    String relateNumber,
    String remark,
  }) =>
      InvoiceDetail(
        sequenceNo: sequenceNo ?? this.sequenceNo,
        itemName: itemName ?? this.itemName,
        qty: qty ?? this.qty,
        unit: unit ?? this.unit,
        unitPrice: unitPrice ?? this.unitPrice,
        salesAmt: salesAmt ?? this.salesAmt,
        taxAmt: taxAmt ?? this.taxAmt,
        totalAmt: totalAmt ?? this.totalAmt,
        discountAmt: discountAmt ?? this.discountAmt,
        healthAmt: healthAmt ?? this.healthAmt,
        relateNumber: relateNumber ?? this.relateNumber,
        remark: remark ?? this.remark,
      );

  factory InvoiceDetail.fromRawJson(String str) =>
      InvoiceDetail.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory InvoiceDetail.fromJson(Map<String, dynamic> json) => InvoiceDetail(
        sequenceNo: json["SequenceNo"] == null ? null : json["SequenceNo"],
        itemName: json["ItemName"] == null ? null : json["ItemName"],
        qty: json["Qty"] == null ? null : json["Qty"],
        unit: json["Unit"] == null ? null : json["Unit"],
        unitPrice: json["UnitPrice"] == null ? null : json["UnitPrice"],
        salesAmt: json["SalesAmt"] == null ? null : json["SalesAmt"],
        taxAmt: json["TaxAmt"] == null ? null : json["TaxAmt"],
        totalAmt: json["TotalAmt"] == null ? null : json["TotalAmt"],
        discountAmt: json["DiscountAmt"] == null ? null : json["DiscountAmt"],
        healthAmt: json["HealthAmt"] == null ? null : json["HealthAmt"],
        relateNumber:
            json["RelateNumber"] == null ? null : json["RelateNumber"],
        remark: json["Remark"] == null ? null : json["Remark"],
      );

  Map<String, dynamic> toJson() => {
        "SequenceNo": sequenceNo == null ? null : sequenceNo,
        "ItemName": itemName == null ? null : itemName,
        "Qty": qty == null ? null : qty,
        "Unit": unit == null ? null : unit,
        "UnitPrice": unitPrice == null ? null : unitPrice,
        "SalesAmt": salesAmt == null ? null : salesAmt,
        "TaxAmt": taxAmt == null ? null : taxAmt,
        "TotalAmt": totalAmt == null ? null : totalAmt,
        "DiscountAmt": discountAmt == null ? null : discountAmt,
        "HealthAmt": healthAmt == null ? null : healthAmt,
        "RelateNumber": relateNumber == null ? null : relateNumber,
        "Remark": remark == null ? null : remark,
      };
}
