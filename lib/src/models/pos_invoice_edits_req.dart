// To parse this JSON data, do
//
//     final posInvoiceEditsReq = posInvoiceEditsReqFromJson(jsonString);

import 'dart:convert';

class PosInvoiceEditsReq {
  PosInvoiceEditsReq({
    this.posBan,
    this.apiKey,
    this.sellerBan,
    this.storeCode,
    this.storeName,
    this.registerCode,
    this.orderNo,
    this.state,
    this.invoiceNo,
    this.invoiceDate,
    this.cancelDate,
    this.buyerBan,
  });

  String posBan;
  String apiKey;
  String sellerBan;
  String storeCode;
  String storeName;
  String registerCode;
  String orderNo;
  num state;
  String invoiceNo;
  String invoiceDate;
  String cancelDate;
  String buyerBan;

  PosInvoiceEditsReq copyWith({
    String posBan,
    String apiKey,
    String sellerBan,
    String storeCode,
    String storeName,
    String registerCode,
    String orderNo,
    num state,
    String invoiceNo,
    String invoiceDate,
    String cancelDate,
    String buyerBan,
  }) =>
      PosInvoiceEditsReq(
        posBan: posBan ?? this.posBan,
        apiKey: apiKey ?? this.apiKey,
        sellerBan: sellerBan ?? this.sellerBan,
        storeCode: storeCode ?? this.storeCode,
        storeName: storeName ?? this.storeName,
        registerCode: registerCode ?? this.registerCode,
        orderNo: orderNo ?? this.orderNo,
        state: state ?? this.state,
        invoiceNo: invoiceNo ?? this.invoiceNo,
        invoiceDate: invoiceDate ?? this.invoiceDate,
        cancelDate: cancelDate ?? this.cancelDate,
        buyerBan: buyerBan ?? this.buyerBan,
      );

  factory PosInvoiceEditsReq.fromRawJson(String str) =>
      PosInvoiceEditsReq.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory PosInvoiceEditsReq.fromJson(Map<String, dynamic> json) =>
      PosInvoiceEditsReq(
        posBan: json["PosBAN"] == null ? null : json["PosBAN"],
        apiKey: json["ApiKey"] == null ? null : json["ApiKey"],
        sellerBan: json["SellerBAN"] == null ? null : json["SellerBAN"],
        storeCode: json["StoreCode"] == null ? null : json["StoreCode"],
        storeName: json["StoreName"] == null ? null : json["StoreName"],
        registerCode:
            json["RegisterCode"] == null ? null : json["RegisterCode"],
        orderNo: json["OrderNo"] == null ? null : json["OrderNo"],
        state: json["State"] == null ? null : json["State"],
        invoiceNo: json["InvoiceNo"] == null ? null : json["InvoiceNo"],
        invoiceDate: json["InvoiceDate"] == null ? null : json["InvoiceDate"],
        cancelDate: json["CancelDate"] == null ? null : json["CancelDate"],
        buyerBan: json["BuyerBAN"] == null ? null : json["BuyerBAN"],
      );

  Map<String, dynamic> toJson() => {
        "PosBAN": posBan == null ? null : posBan,
        "ApiKey": apiKey == null ? null : apiKey,
        "SellerBAN": sellerBan == null ? null : sellerBan,
        "StoreCode": storeCode == null ? null : storeCode,
        "StoreName": storeName == null ? null : storeName,
        "RegisterCode": registerCode == null ? null : registerCode,
        "OrderNo": orderNo == null ? null : orderNo,
        "State": state == null ? null : state,
        "InvoiceNo": invoiceNo == null ? null : invoiceNo,
        "InvoiceDate": invoiceDate == null ? null : invoiceDate,
        "CancelDate": cancelDate == null ? null : cancelDate,
        "BuyerBAN": buyerBan == null ? null : buyerBan,
      };
}
