// To parse this JSON data, do
//
//     final getInvoiceStatusReq = getInvoiceStatusReqFromJson(jsonString);

import 'dart:convert';

class GetInvoiceStatusReq {
  GetInvoiceStatusReq({
    this.invoiceNo,
    this.invoiceDate,
  });

  String invoiceNo;
  String invoiceDate;

  GetInvoiceStatusReq copyWith({
    String invoiceNo,
    String invoiceDate,
  }) =>
      GetInvoiceStatusReq(
        invoiceNo: invoiceNo ?? this.invoiceNo,
        invoiceDate: invoiceDate ?? this.invoiceDate,
      );

  factory GetInvoiceStatusReq.fromRawJson(String str) =>
      GetInvoiceStatusReq.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory GetInvoiceStatusReq.fromJson(Map<String, dynamic> json) =>
      GetInvoiceStatusReq(
        invoiceNo: json["InvoiceNo"] == null ? null : json["InvoiceNo"],
        invoiceDate: json["InvoiceDate"] == null ? null : json["InvoiceDate"],
      );

  Map<String, dynamic> toJson() => {
        "InvoiceNo": invoiceNo == null ? null : invoiceNo,
        "InvoiceDate": invoiceDate == null ? null : invoiceDate,
      };
}
