import 'dart:convert';
import 'dart:math';

import 'models/pos_invoice_edits_req.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:xml/xml.dart';
import 'package:okshop_common/okshop_common.dart';
import 'models/get_invoice_status_req.dart';
import 'models/get_invoice_status_res.dart';
import 'models/pos_invoice_edits_res.dart';
import 'models/pos_invoice_news_req.dart';
import 'models/pos_invoice_news_res.dart';

class InvoiceProvider {
  final Dio dio;
  final String apiKey;
  final String posBAN;
  final String baseUrl;
  final String soapUrl;

  const InvoiceProvider({
    @required this.dio,
    @required this.apiKey,
    @required this.posBAN,
    @required this.baseUrl,
    @required this.soapUrl,
  });

  static String _getSoapXmlString({
    String key,
    String main, // 總公司統編
    @required num year,
    @required num month,
    @required String seller,
    String machineNo = 'M1', // 機台號碼
    num invoiceType = 35,
  }) {
    return '''
    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
    <soap:Body>
        <GetInvoiceRange xmlns="http://tempuri.org/">
            <machineNo>$machineNo</machineNo>
            <APIKey>$key</APIKey>
            <deptCode></deptCode>
            <invoiceYear>$year</invoiceYear>
            <MainBAN>$main</MainBAN>
            <invoiceMonth>$month</invoiceMonth>
            <sellerBAN>$seller</sellerBAN>
            <invoiceType>$invoiceType</invoiceType>
        </GetInvoiceRange>
    </soap:Body>
    </soap:Envelope>
    ''';
  }

  ///
  /// 取得發票號碼
  ///
  Future<String> getInvoiceNumber({
    final String seller,
    final DateTime date,
  }) {
    final uri = Uri.parse(this.soapUrl);
    this.dio.options.contentType = 'text/xml';
    final xmlString = _getSoapXmlString(
      year: date.year,
      month: date.month,
      machineNo: 'M1',
      seller: seller ?? '',
      main: this.posBAN,
      key: this.apiKey,
    );
    return this.dio.postUri<String>(uri, data: xmlString).then(
      (value) {
        final doc = XmlDocument.parse(value.data);
        final it = doc.findAllElements('GetInvoiceRangeResult');
        if (it.length > 0) {
          final root = it.first;
          final result = root?.findElements('Result')?.first?.text ?? '';
          if (RegExp(r'OK').hasMatch(result)) {
            final invoiceNo =
                root?.findElements('InvoiceStart')?.first?.text ?? '';
            if (invoiceNo.isNotEmpty) {
              return Future<String>.value(invoiceNo);
            }
          }
          final msg = root?.findElements('ResultMsg')?.first?.text ?? '';
          return Future<String>.error('加值中心: $msg');
        }
        return Future<String>.error('xml parse error');
      },
      onError: (error, stackTrace) {
        return Future<String>.error(error, stackTrace);
      },
    ).whenComplete(() {
      this.dio.options.contentType = Headers.jsonContentType;
    });
  }

  ///
  /// 查詢發票狀態(單筆)
  ///
  Future<GetInvoiceStatusRes> getInvoiceStatus(GetInvoiceStatusReq data) {
    final uri = Uri.parse('$baseUrl/GetInvoiceStatus');
    // final uri =
    //     Uri.parse('https://www.bpscm.com.tw/SCMWebAPI/api/GetInvoiceStatus');
    return this.dio.postUri<Map>(uri, data: data).then(
      (value) {
        return GetInvoiceStatusRes.fromJson(value.data);
      },
      onError: (error, stackTrace) {
        return Future<GetInvoiceStatusRes>.error(error, stackTrace);
      },
    );
  }

  ///
  /// 開立發票/折讓 (單張)
  ///
  Future<PosInvoiceNewsRes> postPosInvoiceNewsSingle(PosInvoiceNewsReq data) {
    return this.postPosInvoiceNews([data]).then(
      (value) {
        final ret = value.first;
        if ('NG' == ret.status) {
          return Future<PosInvoiceNewsRes>.error(ret.message);
        }
        return value.first;
      },
    );
  }

  ///
  /// 開立發票/折讓
  ///
  Future<Iterable<PosInvoiceNewsRes>> postPosInvoiceNews(
      Iterable<PosInvoiceNewsReq> data) {
    final uri = Uri.parse('$baseUrl/PosInvoiceNews');
    this.dio.options.contentType = Headers.jsonContentType;
    return this.dio.postUri<String>(uri, data: data).then(
      (value) {
        final ls = jsonDecode(value.data);
        return List.from(ls).map((e) => PosInvoiceNewsRes.fromJson(e));
      },
      onError: (error, stackTrace) {
        return Future<Iterable<PosInvoiceNewsRes>>.error(error, stackTrace);
      },
    );
  }

  ///
  /// 作廢發票/折讓 (單張)
  ///
  Future<PosInvoiceEditsRes> postPosInvoiceEditsSingle(
      PosInvoiceEditsReq data) {
    return postPosInvoiceEdits([data]).then(
      (value) {
        final ret = value.first;
        if ('NG' == ret.status) {
          return Future<PosInvoiceEditsRes>.error(ret.message);
        }
        return value.first;
      },
    );
  }

  ///
  /// 作廢發票/折讓
  ///
  Future<Iterable<PosInvoiceEditsRes>> postPosInvoiceEdits(
      Iterable<PosInvoiceEditsReq> data) {
    final uri = Uri.parse('$baseUrl/PosInvoiceEdits');
    this.dio.options.contentType = Headers.jsonContentType;
    return this.dio.postUri<String>(uri, data: data).then(
      (value) {
        final ls = jsonDecode(value.data);
        return List.from(ls).map((e) => PosInvoiceEditsRes.fromJson(e));
      },
      onError: (error, stackTrace) {
        return Future<Iterable<PosInvoiceEditsRes>>.error(error, stackTrace);
      },
    );
  }

  static TaxRate getTaxRate(bool hasBuyer, BpscmTaxType taxType) {
    // 有買方，才有稅率
    if (hasBuyer) {
      // 由稅別決定稅率
      return taxType.isTX ? TaxRate.TX : TaxRate.None;
    }
    return TaxRate.None;
  }

  static String genRandomNumber() {
    // HACK:
    // return '3361';
    final randomNumber = Random().nextInt(10000);
    return '$randomNumber'.padLeft(4, '0');
  }

  /// 查詢發票狀態(多筆) GetInvoiceStatusS
  /// 查詢折讓狀態(多筆) GetAllowanceStatusS
  /// 取得發票大區間(財政部大平台配發) GetInvoiceNo

  // Future editInvoice({
  //   String seller,
  //   String orderNo,
  //   String invoiceNo,
  //   String invoiceDate,
  //   String cancelDate,
  //   String buyer,
  // }) {
  //   final req = InvoiceEditing(
  //     posBan: this.posBAN,
  //     apiKey: this.apiKey,
  //     state: 2,
  //     sellerBan: seller,
  //     storeCode: '',
  //     storeName: '',
  //     registerCode: '',
  //     orderNo: orderNo ?? '',
  //     invoiceNo: invoiceNo,
  //     invoiceDate: invoiceDate,
  //     // allowanceDate: '',
  //     cancelDate: cancelDate,
  //     buyerBan: buyer ?? '',
  //   );
  //   final data = jsonEncode([req.toJson()]);
  //   final uri = Uri.parse('$baseUrl/PosInvoiceEdits');
  //   // final uri = Uri.http('61.57.230.103', 'SCMWebAPITest/api/PosInvoiceEdits');
  //   this.dio.options.contentType = Headers.jsonContentType;
  //   return this
  //       .dio
  //       .postUri<String>(
  //         uri,
  //         data: data,
  //       )
  //       .then(
  //     (value) {
  //       kLogger.d(value.data);
  //       return Future.value(value);
  //     },
  //     onError: (error) {
  //       kLogger.e(error);
  //       return Future<Iterable<InvoiceCreated>>.error(error);
  //     },
  //   );
  // }

  // Future<Iterable<InvoiceCreated>> pushInvoice({
  //   String invoiceNo,
  //   String seller,
  //   String buyer,
  //   String randomNumber,
  //   String orderNo,
  //   String invoiceDate,
  //   String itemName,
  //   num taxType,
  //   num price,
  // }) {
  //   buyer ??= '';
  //   final taxRateNormal = buyer.isNotEmpty ? kTaxRateNormal : 0.0;
  //   var taxRate = taxType == 1 ? taxRateNormal : kTaxRateFree;
  //   final untaxPrice = price / (1.0 + taxRate);
  //   final untaxPriceRound = untaxPrice.round(); // 3600 = 3780 / 1.05
  //   final taxAmt = (untaxPrice * taxRate).round(); // 180 = 3600 * 0.05
  //   taxRate = taxType == 1 ? kTaxRateNormal : kTaxRateFree;
  //   num salesAmt = 0; // 應稅
  //   if (1 == taxType) {
  //     salesAmt = untaxPriceRound; // 3600 = 3780 - 180
  //   }
  //   num freeTaxSalesAmt = 0; // 免稅
  //   if (3 == taxType) {
  //     freeTaxSalesAmt = untaxPriceRound;
  //   }
  //   final zeroTaxSalesAmt = 0; // 零稅
  //   final invoiceType = buyer.isNotEmpty ? 25 : 35; // 有買方統編，格式25
  //   final req = InvoiceCreating(
  //     posBan: this.posBAN,
  //     apiKey: this.apiKey,
  //     state: 1, // 1: 發票開立, 4: 折讓開立
  //     sellerBan: seller, // 賣方統編
  //     storeCode: '',
  //     storeName: '',
  //     registerCode: '',
  //     orderNo: orderNo ?? '',
  //     invoiceNo: invoiceNo,
  //     invoiceDate: invoiceDate,
  //     // allowanceDate: '',
  //     buyerBan: buyer ?? '',
  //     printMark: 'Y',
  //     // memberId: '',
  //     // checkNo: '',
  //     // invoiceType: '35',
  //     invoiceType: '$invoiceType',
  //     // groupMark: '',
  //     salesAmt: salesAmt, // 3,600
  //     freeTaxSalesAmt: freeTaxSalesAmt,
  //     zeroTaxSalesAmt: zeroTaxSalesAmt,
  //     taxAmt: taxAmt, // 180
  //     totalAmt: price, // 3,780
  //     taxType: '${taxType ?? 1}',
  //     taxRate: taxRate,
  //     discountAmt: 0,
  //     healthyAmt: 0,
  //     // carrierType: '',
  //     // carrierId1: '',
  //     // carrierId2: '',
  //     // npoBan: '',
  //     randomNumber: randomNumber,
  //     // mainRemark: '',
  //     // formatType: '',
  //   );
  //   final item = InvoiceDetail(
  //     sequenceNo: '',
  //     itemName: (itemName ?? '').isNotEmpty ? itemName : kDefaultItemName,
  //     qty: 1,
  //     // unit: '',
  //     unitPrice: price, // 單價 (總額)
  //     salesAmt: untaxPriceRound, // 銷售額 (未稅價)
  //     taxAmt: taxAmt,
  //     totalAmt: price,
  //     discountAmt: 0,
  //     healthAmt: 0,
  //     // relateNumber: '',
  //     // remark: '',
  //   );
  //   req.invoiceDetails ??= [item];
  //   final uri = Uri.parse('$baseUrl/PosInvoiceNews');
  //   // final uri = Uri.http('61.57.230.103', 'SCMWebAPITest/api/PosInvoiceNews');
  //   this.dio.options.contentType = 'application/json';
  //   final data = jsonEncode([req.toJson()]);
  //   kLogger.d(data);
  //   this.dio.options.contentType = Headers.jsonContentType;
  //   return this.dio.postUri<String>(uri, data: data).then(
  //     (value) {
  //       // kLogger.d('');
  //       final it = jsonDecode(value.data) as Iterable;
  //       final ls = it.map((e) => InvoiceCreated.fromJson(e));
  //       return Future.value(ls);
  //     },
  //     onError: (error) {
  //       kLogger.e(error);
  //       if (error is DioError) {
  //         return Future<Iterable<InvoiceCreated>>.error(error.message);
  //       }
  //       return Future<Iterable<InvoiceCreated>>.error(error);
  //     },
  //   );
  // }
}
