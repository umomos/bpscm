import 'package:bpscm/src/invoice_provider.dart';
import 'package:flutter_test/flutter_test.dart';

main() {
  group('string', () {
    test('padLeft', () {
      expect('123'.padLeft(4, '0'), '0123');
    });

    test('padRight', () {
      expect('123'.padRight(4, '0'), '1230');
    });

    test('random', () {
      final randomString = InvoiceProvider.genRandomNumber();
      expect(randomString.length, 4);
    });
  });
}
