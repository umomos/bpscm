import 'package:bpscm/bpscm.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:okshop_common/okshop_common.dart';

main() {
  const _posBAN = '83193989';
  group('tax rate', () {
    PosInvoiceNewsReq invoice;
    InvoiceDetail item;
    final items = [
      InvoiceDetail(
        itemName: '可樂',
        totalAmt: 100,
        qty: 1,
        remark: BpscmTaxType.TX.str,
      ),
      InvoiceDetail(
        itemName: '雞蛋',
        totalAmt: 200,
        qty: 1,
        remark: BpscmTaxType.Free.str,
      ),
      InvoiceDetail(
        itemName: '現場折扣',
        discountAmt: -100,
        totalAmt: 0,
        qty: 1,
        remark: BpscmTaxType.TX.str,
      ),
    ];

    setUp(() {
      item = InvoiceDetail(
        totalAmt: 9450,
        qty: 50,
      );
      invoice = PosInvoiceNewsReq(
        totalAmt: 9450,
      );
    });

    test('tax type free with buyer', () {
      invoice.buyerBan = _posBAN;
      invoice.invoiceDetails = items;
      invoice.refresh();
      expect(invoice.taxType, BpscmTaxType.Free.str);
      expect(invoice.taxAmt, 0);
      expect(invoice.salesAmt, 0);
      expect(invoice.freeTaxSalesAmt, 200);
    });

    test('tax type mix without buyer', () {
      invoice.buyerBan = '';
      invoice.invoiceDetails = items;
      invoice.refresh();
      expect(invoice.taxType, BpscmTaxType.Free.str);
      expect(invoice.taxAmt, 0);
      expect(invoice.salesAmt, 0);
      expect(invoice.freeTaxSalesAmt, 200);
    });

    test('invoice normal with buyer', () {
      item.remark = BpscmTaxType.TX.str;
      invoice.buyerBan = _posBAN;
      invoice.invoiceDetails = [item];
      invoice.refresh();
      expect(invoice.taxType, item.remark);
      expect(invoice.taxAmt, 450);
      expect(invoice.salesAmt, 9000);
    });

    test('invoice normal without buyer', () {
      item.remark = BpscmTaxType.TX.str;
      invoice.buyerBan = '';
      invoice.invoiceDetails = [item];
      invoice.refresh();
      expect(invoice.taxType, item.remark);
      expect(invoice.taxAmt, 0);
      expect(invoice.salesAmt, 9450);
    });

    test('invoice free without buyer', () {
      item.remark = BpscmTaxType.Free.str;
      invoice.buyerBan = '';
      invoice.invoiceDetails = [item];
      invoice.refresh();
      expect(invoice.taxType, item.remark);
      expect(invoice.taxAmt, 0);
      expect(invoice.salesAmt, 0);
      expect(invoice.freeTaxSalesAmt, 9450);
    });

    test('invoice free with buyer', () {
      item.remark = BpscmTaxType.Free.str;
      invoice.buyerBan = _posBAN;
      invoice.invoiceDetails = [item];
      invoice.refresh();
      expect(invoice.taxType, item.remark);
      expect(invoice.taxAmt, 0);
      expect(invoice.salesAmt, 0);
      expect(invoice.freeTaxSalesAmt, 9450);
    });

    test('item with buyer and tax normal', () {
      item.remark = BpscmTaxType.TX.str;
      item.refresh(true);
      expect(item.unitPrice, 180);
      expect(item.taxAmt, 450);
      expect(item.salesAmt, 9000);
    });

    test('item without buyer and tax normal', () {
      item.remark = BpscmTaxType.TX.str;
      item.refresh(false);
      expect(item.unitPrice, 189);
      expect(item.taxAmt, 0);
      expect(item.salesAmt, 9450);
    });

    test('item with buyer and tax free', () {
      item.remark = BpscmTaxType.Free.str;
      item.refresh(true);
      expect(item.unitPrice, 189);
      expect(item.taxAmt, 0);
      expect(item.salesAmt, 9450);
    });

    test('item without buyer and tax free', () {
      item.remark = BpscmTaxType.Free.str;
      item.refresh(false);
      expect(item.unitPrice, 189);
      expect(item.taxAmt, 0);
      expect(item.salesAmt, 9450);
    });
  });

  group('稅額計算', () {
    test('狀況一：當產品都是應稅時', () {
      final item1 = InvoiceDetail(
        itemName: 'A商品',
        totalAmt: 300,
        qty: 1,
        remark: BpscmTaxType.TX.str,
      );
      final item2 = InvoiceDetail(
        itemName: 'B商品',
        totalAmt: 200,
        qty: 1,
        remark: BpscmTaxType.TX.str,
      );
      final item3 = InvoiceDetail(
        itemName: '現場折扣',
        discountAmt: -200.0,
        qty: 1,
        remark: BpscmTaxType.TX.str,
      );

      final invoice = PosInvoiceNewsReq();
      invoice.invoiceDetails = [item1, item2, item3];
      invoice.buyerBan = '83193989';
      invoice.refresh();
      expect(invoice.taxType, '${BpscmTaxType.TX.value}');
      expect(invoice.itemsFreeSalesAmount.round(), 0);
      expect(invoice.itemsTXSalesAmount.round(), 286);
      expect(invoice.itemsSalesAmount.round(), 286);
      expect(invoice.itemsTaxAmount.round(), 14);
      expect(invoice.itemsTotalAmount.round(), 300);
    });
  });

  group('狀況二：當商品包含應稅＆免稅時', () {
    final invoice = PosInvoiceNewsReq();
    test('2-1. 折扣金額 > 應稅商品', () {
      final item1 = InvoiceDetail(
        itemName: 'A商品',
        qty: 1,
        totalAmt: 300.0,
        remark: BpscmTaxType.Free.str,
      );
      final item2 = InvoiceDetail(
        itemName: 'B商品',
        qty: 1,
        totalAmt: 200.0,
        remark: BpscmTaxType.TX.str,
      );
      final item3 = InvoiceDetail(
        itemName: '現場折扣',
        qty: 1,
        discountAmt: -100.0,
        remark: BpscmTaxType.TX.str,
      );

      invoice.buyerBan = '83193989';
      invoice.invoiceDetails = [item1, item2, item3];
      invoice.refresh();
      expect(invoice.taxType, '${BpscmTaxType.Mix.value}');
      expect(invoice.itemsFreeSalesAmount.round(), 300);
      expect(invoice.itemsTXSalesAmount.round(), 95);
      expect(invoice.itemsSalesAmount.round(), 395);
      expect(invoice.itemsTaxAmount.round(), 5);
      expect(invoice.itemsTotalAmount.round(), 400);
    });

    test('2-2. 折扣金額 > 應稅商品', () {
      final item1 = InvoiceDetail(
        itemName: 'A商品',
        qty: 1,
        totalAmt: 300.0,
        remark: BpscmTaxType.Free.str,
      );
      final item2 = InvoiceDetail(
        itemName: 'B商品',
        qty: 1,
        totalAmt: 200.0,
        remark: BpscmTaxType.TX.str,
      );
      final item3 = InvoiceDetail(
        itemName: 'C商品',
        qty: 1,
        totalAmt: 300.0,
        remark: BpscmTaxType.TX.str,
      );
      final item4 = InvoiceDetail(
        itemName: '現場折扣',
        qty: 1,
        discountAmt: -400.0,
        remark: BpscmTaxType.TX.str,
      );
      invoice.buyerBan = '83193989';
      invoice.invoiceDetails = [item1, item2, item3, item4];
      invoice.refresh();
      expect(invoice.taxType, '${BpscmTaxType.Mix.value}');
      expect(invoice.itemsFreeSalesAmount.round(), 300);
      expect(invoice.itemsTXSalesAmount.round(), 95);
      expect(invoice.itemsSalesAmount.round(), 395);
      expect(invoice.itemsTaxAmount.round(), 5);
      expect(invoice.itemsTotalAmount.round(), 400);
    });

    test('2-3. 折扣金額 > 應稅商品', () {
      final item1 = InvoiceDetail(
        itemName: 'A商品',
        qty: 1,
        totalAmt: 300.0,
        remark: BpscmTaxType.Free.str,
      );
      final item2 = InvoiceDetail(
        itemName: 'B商品',
        qty: 1,
        totalAmt: 200.0,
        remark: BpscmTaxType.TX.str,
      );
      final item3 = InvoiceDetail(
        itemName: 'C商品',
        qty: 1,
        totalAmt: 300.0,
        remark: BpscmTaxType.TX.str,
      );
      final item4 = InvoiceDetail(
        itemName: '現場折扣',
        qty: 1,
        discountAmt: -600.0,
        remark: BpscmTaxType.TX.str,
      );
      invoice.buyerBan = '83193989';
      invoice.invoiceDetails = [item1, item2, item3, item4];
      invoice.refresh();
      expect(invoice.taxType, '${BpscmTaxType.Free.value}');
      expect(invoice.itemsFreeSalesAmount.round(), 300);
      expect(invoice.itemsTXSalesAmount.round(), 0);
      expect(invoice.itemsSalesAmount.round(), 200);
      expect(invoice.itemsTaxAmount.round(), 0);
      expect(invoice.itemsTotalAmount.round(), 200);
      expect(invoice.totalAmt.round(), 200);
    });
  });

  group('狀況三：無買家，當商品包含應稅＆免稅時', () {
    final invoice = PosInvoiceNewsReq();
    test('2-1. 折扣金額 > 應稅商品', () {
      final item1 = InvoiceDetail(
        itemName: 'A商品',
        qty: 1,
        totalAmt: 300.0,
        remark: BpscmTaxType.Free.str,
      );
      final item2 = InvoiceDetail(
        itemName: 'B商品',
        qty: 1,
        totalAmt: 200.0,
        remark: BpscmTaxType.TX.str,
      );
      final item3 = InvoiceDetail(
        itemName: '現場折扣',
        qty: 1,
        discountAmt: -100.0,
        remark: BpscmTaxType.TX.str,
      );
      // invoice.buyerBan = '83193989';
      invoice.invoiceDetails = [item1, item2, item3];
      invoice.refresh();
      expect(invoice.taxType, BpscmTaxType.Mix.str);
      expect(invoice.itemsFreeSalesAmount.round(), 300);
      expect(invoice.itemsTXSalesAmount.round(), 100);
      expect(invoice.itemsSalesAmount.round(), 400);
      expect(invoice.itemsTaxAmount.round(), 0);
      expect(invoice.itemsTotalAmount.round(), 400);
    });

    test('2-2. 折扣金額 > 應稅商品', () {
      final item1 = InvoiceDetail(
        itemName: 'A商品',
        qty: 1,
        totalAmt: 300.0,
        remark: BpscmTaxType.Free.str,
      );
      final item2 = InvoiceDetail(
        itemName: 'B商品',
        qty: 1,
        totalAmt: 200.0,
        remark: BpscmTaxType.TX.str,
      );
      final item3 = InvoiceDetail(
        itemName: 'C商品',
        qty: 1,
        totalAmt: 300.0,
        remark: BpscmTaxType.TX.str,
      );
      final item4 = InvoiceDetail(
        itemName: '現場折扣',
        qty: 1,
        discountAmt: -400.0,
        remark: BpscmTaxType.TX.str,
      );
      // invoice.buyerBan = '83193989';
      invoice.invoiceDetails = [item1, item2, item3, item4];
      invoice.refresh();
      expect(invoice.taxType, BpscmTaxType.Mix.str);
      expect(invoice.itemsFreeSalesAmount.round(), 300);
      expect(invoice.itemsTXSalesAmount.round(), 100);
      expect(invoice.itemsSalesAmount.round(), 400);
      expect(invoice.itemsTaxAmount.round(), 0);
      expect(invoice.itemsTotalAmount.round(), 400);
    });

    test('2-3. 折扣金額 > 應稅商品', () {
      final item1 = InvoiceDetail(
        itemName: 'A商品',
        qty: 1,
        totalAmt: 300.0,
        remark: BpscmTaxType.Free.str,
      );
      final item2 = InvoiceDetail(
        itemName: 'B商品',
        qty: 1,
        totalAmt: 200.0,
        remark: BpscmTaxType.TX.str,
      );
      final item3 = InvoiceDetail(
        itemName: 'C商品',
        qty: 1,
        totalAmt: 300.0,
        remark: BpscmTaxType.TX.str,
      );
      final item4 = InvoiceDetail(
        itemName: '現場折扣',
        qty: 1,
        discountAmt: -600.0,
        remark: BpscmTaxType.TX.str,
      );
      // invoice.buyerBan = '83193989';
      invoice.invoiceDetails = [item1, item2, item3, item4];
      invoice.refresh();
      expect(invoice.taxType, BpscmTaxType.Free.str);
      expect(invoice.itemsFreeSalesAmount.round(), 300);
      expect(invoice.itemsTXSalesAmount.round(), 0);
      expect(invoice.itemsSalesAmount.round(), 200);
      expect(invoice.itemsTaxAmount.round(), 0);
      expect(invoice.itemsTotalAmount.round(), 200);
      expect(invoice.totalAmt.round(), 200);
    });
  });
}
