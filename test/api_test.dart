import 'package:bpscm/bpscm.dart';
import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:okshop_common/okshop_common.dart';

const API_KEY = 'S7MTTXEE-S7MT-S7MT-S7MT-S7MTTXEEM8HY';
const POS_BAN = '83193989';
const SELLER = '83193989';
const BASE_URL = 'https://webtest.bpscm.com.tw/SCMWEBAPI/API';
const SOAP_URL =
    'https://webtest.bpscm.com.tw/WebService_Npoi2/PosInvoiceRange.asmx?WSDL';

main() {
  InvoiceProvider invoiceProvider;
  group('invoice', () {
    setUp(() {
      invoiceProvider = InvoiceProvider(
        dio: Dio(),
        apiKey: API_KEY,
        posBAN: POS_BAN,
        baseUrl: BASE_URL,
        soapUrl: SOAP_URL,
      );
    });

    tearDown(() {
      invoiceProvider = null;
    });

    test('GetInvoiceStatus', () async {
      // expectAsync0(callback)
      // expectAsyncUntil0(callback, isDone)
      // expectLater(actual, matcher)
      // expectSync(actual, matcher)
      final data = GetInvoiceStatusReq(
        invoiceNo: 'AB00000001',
        invoiceDate: '2016/02/18',
      );
      final res = await invoiceProvider.getInvoiceStatus(data);
      expect(res.status, '折讓開立');
    });

    test('GetInvoiceNumber', () async {
      try {
        final date = DateTime(2021, 4);
        final invoiceNo = await invoiceProvider.getInvoiceNumber(
          seller: '83193989',
          date: date,
        );
        expect(invoiceNo.length, 10);
      } catch (err) {
        print('$err');
      }
    });

    test('CreateInvoice', () async {
      try {
        final now = DateTime.now();
        final year = now.year;
        final month = now.month;
        final day = now.day;
        // 如果取不到發票號碼，需使用 '新增發票中區間(/AddPosInvoiceRange)' 新增
        final invoiceNo = await invoiceProvider.getInvoiceNumber(
          seller: SELLER,
          // date: DateTime(2021, 8),
          date: now,
        );
        final item = InvoiceDetail(
          sequenceNo: '',
          itemName: '商品一批',
          qty: 1,
          unit: '',
          unitPrice: 220,
          salesAmt: 220,
          taxAmt: 0,
          totalAmt: 220,
          discountAmt: 0,
          healthAmt: 0,
          relateNumber: '',
          remark: '',
        );
        final data = PosInvoiceNewsReq(
          posBan: POS_BAN,
          apiKey: API_KEY,
          sellerBan: SELLER,
          storeCode: '',
          storeName: '',
          registerCode: '',
          orderNo: '',
          // state: 1,
          state: BpscmInvoiceStatus.Invoice.value,
          invoiceNo: invoiceNo,
          // invoiceDate: '2021/04/21',
          invoiceDate: '$year/$month/$day',
          // allowanceDate: '',
          buyerBan: '',
          printMark: 'Y',
          // memberId: '',
          // checkNo: '',
          invoiceType: '35',
          groupMark: 'Y',
          salesAmt: 220,
          freeTaxSalesAmt: 0,
          zeroTaxSalesAmt: 0,
          taxAmt: 0,
          totalAmt: 220,
          // taxType: '1',
          taxType: '${BpscmTaxType.TX.value}',
          taxRate: 0.05,
          discountAmt: 0,
          healthyAmt: 0,
          // carrierType: '',
          // carrierId1: '',
          // carrierId2: '',
          // npoBan: '',
          randomNumber: '0000',
          // mainRemark: '',
          // formatType: '',
          invoiceDetails: [item],
        );
        final result = await invoiceProvider.postPosInvoiceNews([data]);
        expect(result.first.invoiceNo, invoiceNo);
      } catch (err) {
        print('$err');
      }
    });
  });
}
