# bpscm

A new Flutter package project.

## package

```zsh
flutter create --template=package hello
```

```zsh
flutter create --template=plugin -i swift -a kotlin hello
```

### [开发Packages和插件](https://flutterchina.club/developing-packages/)

### [Flutter Packages 的开发和提交](https://flutter.cn/docs/development/packages-and-plugins/developing-packages)

## 金財通

### 金財通後台

* ~~測試 - http://61.57.230.103/ASPTest/default.aspx~~
* 測試 - https://webtest.bpscm.com.tw/ASPTest
* 正式 - https://www.bpscm.com.tw/ASP/Login/Login.aspx

### omos

* 測試
  * 83193989POS
  * 0001
* 正式
  * 83193989POS
  * Posc@t666

## 測試用 API_KEY

* omos - S7MTTXEE-S7MT-S7MT-S7MT-S7MTTXEEM8HY
* okshop - LXTP2UV2-LXTP-LXTP-LXTP-LXTP2UV2IIWS

## 正式用 API_KEY

* omos - S6FR5FQE-S6FR-S6FR-S6FR-S6FR5FQE2SMA

## tax no

* omos - 83193989
* okshop - 29187497

## 開立發票的 api 網址

* ~~測試站 - http://61.57.230.103/SCMWebAPITest/api/~~
* 測試站 - https://webtest.bpscm.com.tw/SCMWEBAPI/API
* 正式站 - https://www.bpscm.com.tw/SCMWebAPI/api/

## 取得發票的 api 網址

* 測試站 - http://61.57.230.103/WebService_Npoi2Test/PosInvoiceRange.asmx
* 正式站 - https://www.bpscm.com.tw/WebService_Npoi2/PosInvoiceRange.asmx?WSDL

## [doc](https://drive.google.com/drive/folders/1WWxfqnkllcdtjXiCQEV3kNt8UVS-p9hA?usp=sharing)

* [金財通_POS配號小區間初版.pdf](https://drive.google.com/file/d/1pmacosVWK-dkr2euz7D727cfG-xHkwjT/view?usp=sharing)

取得發票區間文件

* [金財通_POS電子發票傳輸格式json-v0.9.pdf](https://drive.google.com/file/d/1jOpsAz2mNrkmTvyopvMvGdm1GxRatoc3/view?usp=sharing)

使用 api 的方式開立發票

* [金財通_POS電子發票傳輸格式txt-v1.4單純上傳.pdf](https://drive.google.com/file/d/15DyHObkRsync23hjg_eJ-zlzO1e0bywP/view?usp=sharing)
